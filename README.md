Website: https://jamesray.com/
Address: Henderson, NV
Ph NO: 310-526-8750

James Arthur Ray is a thought leader considered one of the world’s foremost performance advisors, coaches and consultants.  James Arthur Ray is the President and CEO of James Ray International, a multi-million dollar corporation dedicated to teaching individuals to create wealth in all areas of their lives: financially, relationally, mentally, physically and spiritually. 
He is the author of the New York Times Bestseller Harmonic Wealth:  The Secret to Attracting the Life You Want which focuses on his principle of Going Three For Three: how to effectively bring your thoughts, feelings and actions into harmony with bold application. For more information regarding James Arthur Ray’s business consulting, or keynote presentations please contact us today.
